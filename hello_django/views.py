from django.http import JsonResponse

def ping(request):
    data = {'ping': 'ping-pong!'}
    return JsonResponse(data)